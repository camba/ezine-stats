import React from "react";
import "./App.css";
import Stars from "./Stars";
import CustomBar from "./CustomBar";
import CustomPie from "./CustomPie";
import {
  Lectores,
  CuantoLeemos,
  Impresion,
  Utilidad,
  Prioridad,
  NoConocido,
  Comprensible,
  Valores,
  Reflexion,
  Recomendacion
} from "./data";

function App() {
  return (
    <div className="App">
      <h1 className="main-title">Resultado de la encuesta sobre Ezine</h1>
      <div className="graphic">
        <Stars rating={Impresion.data} title={Impresion.title} />
      </div>
      <div className="graphic">
        <CustomBar
          data={Lectores.data}
          title={Lectores.title}
          {...Lectores.config}
        />
      </div>
      <div className="graphic">
        <CustomPie
          data={CuantoLeemos.data}
          title={CuantoLeemos.title}
          {...Impresion.config}
        />
      </div>
      <div className="graphic">
        <CustomPie
          data={Utilidad.data}
          title={Utilidad.title}
          {...Utilidad.config}
        />
      </div>
      <div className="graphic">
        <CustomPie
          data={Prioridad.data}
          title={Prioridad.title}
          {...Prioridad.config}
        />
      </div>
      <div className="graphic">
        <CustomPie
          data={NoConocido.data}
          title={NoConocido.title}
          {...NoConocido.config}
        />
      </div>
      <div className="graphic">
        <CustomPie
          data={Comprensible.data}
          title={Comprensible.title}
          {...Comprensible.config}
        />
      </div>
      <div className="graphic">
        <CustomPie
          data={Valores.data}
          title={Valores.title}
          {...Valores.config}
        />
      </div>
      <div className="graphic">
        <CustomPie
          data={Reflexion.data}
          title={Reflexion.title}
          {...Reflexion.config}
        />
      </div>
      <div className="graphic">
        <CustomPie
          data={Recomendacion.data}
          title={Recomendacion.title}
          {...Recomendacion.config}
        />
      </div>
    </div>
  );
}

export default App;
