import React from "react";
import { ResponsivePie } from "@nivo/pie";
import { DefaultConfigPie as config } from "./config";

class CustomPie extends React.Component {
  render() {
    return (
      <div className="chart">
        <h2 className="sub-title">{this.props.title}</h2>
        <ResponsivePie {...this.props} />
      </div>
    );
  }
}

CustomPie.defaultProps = {
  data: [],
  title: "",
  ...config
};

export default CustomPie;
