const Lectores = {
  config: {
    indexBy: "country",
    keys: [
      "Siempre",
      "A menudo",
      "Ocasionalmente",
      "Nunca"
    ],
    groupMode: "grouped",
    layout: "vertical",
    enableGridY: false,
    enableGridX: true,
  },
  title: "¿Qué Leemos?",
  data: [
    {
      "country": "Frase/Canción/Intro",
      "Siempre": 15,
      "A menudo": 1,
      "Ocasionalmente": 1,
      "Nunca": 1,
    },
    {
      "country": "Herramientas de Software",
      "Siempre": 7,
      "A menudo": 8,
      "Ocasionalmente": 2,
      "Nunca": 1,
    },
    {
      "country": "Informes/Encuestas",
      "Siempre": 4,
      "A menudo": 10,
      "Ocasionalmente": 3,
      "Nunca": 1,
    },
    {
      "country": "Seguridad",
      "Siempre": 9,
      "A menudo": 2,
      "Ocasionalmente": 7,
      "Nunca": 0,
    },

    {
      "country": "Contenido Técnico",
      "Siempre": 5,
      "A menudo": 9,
      "Ocasionalmente": 4,
      "Nunca": 0,
    },
     {
      "country": "Contenido Social",
      "Siempre": 6,
      "A menudo": 8,
      "Ocasionalmente": 3,
      "Nunca": 1,
    },
      {
      "country": "Género",
      "Siempre": 6,
      "A menudo": 8,
      "Ocasionalmente": 4,
      "Nunca": 0,
    },
    {
      "country": "Audio/Entrevista",
      "Siempre": 2,
      "A menudo": 3,
      "Ocasionalmente": 9,
      "Nunca": 4,
    },
  ]
}

const CuantoLeemos = {
  title: "¿Cuanto leemos?",
  data: [
    {
      "id": "Casi todo",
      "label": "80%",
      "value": 11,
    },
    {
      "id": "Algunas cosas",
      "label": "40%",
      "value": 6,
    },
    {
      "id": "Casi nada",
      "label": "20%",
      "value": 1,
    },
  ]
}


const ratings = [ 4, 5, 5, 5, 5, 5, 5, 5, 4, 5, 5, 4, 4, 5, 5, 5, 5, 5 ]
const average = arr => arr.reduce( ( p, c ) => p + c, 0 ) / arr.length;
const rating = average(ratings)

const Impresion = {
  title: 'Impresión',
  data: rating,
  config: {
    widgetDimensions: "40px",
    widgetSpacings: "15px",
  }
}

const Utilidad = {
  title: 'Utilidad',
  data: [
    {
      "id": "Total",
      "label": "Total",
      "value": 6,
    },
    {
      "id": "Mayormente",
      "label": "Mayormente",
      "value": 11,
    },
    {
      "id": "Medianamente",
      "label": "Medianamente",
      "value": 1,
    },
  ],
  config: {
  }
}

const Prioridad = {
  title: 'Proridad sobre otros contenidos',
  data: [
    {
      "id": "Si",
      "label": "Si",
      "value": 13,
    },
    {
      "id": "No",
      "label": "No",
      "value": 5,
    },
  ],
  config: {
  }
}

const NoConocido = {
  title: 'Temas no conocidos',
  data: [
    {
      "id": "Si",
      "label": "Si",
      "value": 18,
    },
    {
      "id": "No",
      "label": "No",
      "value": 0,
    },
  ],
  config: {
  }
}

const Comprensible = {
  title: '¿Es Comprensible?',
  data: [
    {
      "id": "Habitualmente",
      "label": "Habitualmente",
      "value": 13,
    },
    {
      "id": "Siempre",
      "label": "Siempre",
      "value": 5,
    },
  ],
  config: {
  }
}

const Valores = {
  title: 'Representa los Valores',
  data: [
    {
      "id": "Si",
      "label": "Si",
      "value": 17,
    },
    {
      "id": "No",
      "label": "No",
      "value": 0,
    },
  ],
  config: {
  }
}

const Reflexion = {
  title: 'Reflexión',
  data: [
    {
      "id": "Si",
      "label": "Si",
      "value": 17,
    },
    {
      "id": "No",
      "label": "No",
      "value": 1,
    },
  ],
  config: {
  }
}

const Recomendacion = {
  title: 'Recomendación',
  data: [
    {
      "id": "Si",
      "label": "Si",
      "value": 18,
    },
    {
      "id": "No",
      "label": "No",
      "value": 0,
    },
  ],
  config: {
  }
}

export {
  Lectores,
  CuantoLeemos,
  Impresion,
  Utilidad,
  Prioridad,
  NoConocido,
  Comprensible,
  Valores,
  Reflexion,
  Recomendacion
}
