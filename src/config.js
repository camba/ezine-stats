const DefaultConfigBar = {
  indexBy: "",
  keys: [],
  labelSkipWidth: 12,
  labelSkipHeight: 12,
  labelTextColor: "inherit:darker(1.6)",
  animate: true,
  motionStiffness: 90,
  motionDamping: 15,
  axisRight: null,
  colors: { scheme: 'nivo' },
  colorBy: "id",
  borderColor: {from: 'color', modifiers: [ [ 'darker', 1.6 ] ]},
  padding: 0.3,
  margin: {
     top: 50,
     right: 130,
     bottom: 50,
     left: 60
   },
   defs: [
   ],
   fill: [
   ],
   axisBottom: {
     tickSize: 5,
     tickPadding: 5,
     tickRotation: 0,
     legend: "",
     legendPosition: "middle",
     legendOffset: 32
   },
   axisLeft: {
     tickSize: 5,
     tickPadding: 5,
     tickRotation: 0,
     legend: "",
     legendPosition: "middle",
     legendOffset: -40
   },
   axisTop: {
     tickSize: 0,
     tickPadding: 12,
     legend: "",
   },
   legends: [
     {
       dataFrom: "keys",
       anchor: "bottom-right",
       direction: "column",
       justify: false,
       translateX: 120,
       translateY: 0,
       itemsSpacing: 2,
       itemWidth: 100,
       itemHeight: 20,
       itemDirection: "left-to-right",
       itemOpacity: 0.85,
       symbolSize: 20,
       effects: [
         {
           on: "hover",
           style: {
             itemOpacity: 1
           }
         }
       ]
     }
   ]
}

const DefaultConfigPie = {
  margin: { top: 40, right: 80, bottom: 80, left: 80, },
  innerRadius: 0.4,
  enableRadialLabels: false,
  borderWidth: 3,
  borderColor: "#000",
  slicesLabelsSkipAngle: 10,
  animate: true,
  isInteractive: false,
  colors: { scheme: 'nivo' },
  radialLabelsTextXOffset: 6,
  radialLabelsTextColor: "#333333",
  radialLabelsLinkOffset: 0,
  radialLabelsLinkDiagonalLength: 16,
  radialLabelsLinkHorizontalLength: 24,
  radialLabelsLinkStrokeWidth: 1,
  radialLabelsLinkColor: { from: 'color' },
  legends: [
    {
      anchor: 'bottom',
      direction: 'row',
      translateY: 56,
      itemWidth: 100,
      itemHeight: 18,
      itemTextColor: '#999',
      symbolSize: 18,
      symbolShape: 'circle',
      effects: [
        {
          on: 'hover',
          style: {
            itemTextColor: '#000'
          }
        }
      ]
    }
  ]
}

export { DefaultConfigBar, DefaultConfigPie }
