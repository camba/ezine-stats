import React from "react";
import Ratings from "react-ratings-declarative";

class Stars extends React.Component {
  render() {
    return (
      <div className="stars">
        <h2 className="sub-title">{this.props.title}</h2>
        <Ratings {...this.props}>
          <Ratings.Widget widgetRatedColor="#00c79a" />
          <Ratings.Widget widgetRatedColor="#00c79a" />
          <Ratings.Widget widgetRatedColor="#00c79a" />
          <Ratings.Widget widgetRatedColor="#00c79a" />
          <Ratings.Widget widgetRatedColor="#00c79a" />
        </Ratings>
      </div>
    );
  }
}

export default Stars;
