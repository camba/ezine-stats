import React from "react";
import { ResponsiveBar } from "@nivo/bar";
import { DefaultConfigBar as config } from "./config";

class CustomBar extends React.Component {
  render() {
    return (
      <div className="chart">
        <h2 className="sub-title">{this.props.title}</h2>

        <ResponsiveBar {...this.props} />
      </div>
    );
  }
}

CustomBar.defaultProps = {
  data: [],
  title: "",
  ...config
};

export default CustomBar;
