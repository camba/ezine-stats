Proyecto mínimo para mostrar algunos gráficos de los resultados de la encuesta del ezine 2019.

El proyecto se inicio con [Create React App](https://github.com/facebook/create-react-app).

## Instalar dependencias

`yarn install`

## Iniciar Aplicación

`yarn start`

Corre la aplicación en modo desarrollo.<br />
Abre [http://localhost:3000](http://localhost:3000) para que la veas en el navegador.

## Bibliotecas/Librerias Utilizadas

- [React](https://reactjs.org/)
- [Nivo](https://nivo.rocks/)
- [React-ratings-declarative](https://github.com/ekeric13/react-ratings-declarative)
